package assignment2.cameraapp;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

/**
 * Snap image lets us capture an image with the camera
 */
public class SnapImage extends Activity {
	private Camera camera;

	/**
	 * Get the camera and start the camera preview by adding it to the view
	 * Stores an image on button click
	 *
	 * @param savedInstanceState The saved instance state
	 */
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.snap_image);

		camera = getCamera();

		CameraPreview preview = new CameraPreview(this, camera);
		FrameLayout previewLayout = (FrameLayout) findViewById(R.id.camera_preview);
		previewLayout.addView(preview);

		Button snapImageButton = (Button) findViewById(R.id.snapImageButton);
		snapImageButton.setOnClickListener(
			new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					camera.takePicture(null, null, image);
				}
			}
		);
	}

	/**
	 * Gets the devices camera by attempting to open it
	 *
	 * @return The camera
	 */
	public static Camera getCamera() {
		Camera camera = null;
		try{
			camera = Camera.open(0);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return camera;
	}

	/**
	 * When a picture was taken this is the callback, and we will send the picture to the activity StoreImage to store it onto the device
	 */
	private Camera.PictureCallback image = new Camera.PictureCallback() {
		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			Intent intent = new Intent(getApplication(), StoreImage.class);
			intent.putExtra("image", data);
			startActivity(intent);
		}
	};

	/**
	 * Make sure we hold on to the camera by releasing it on pause
	 */
	@Override
	protected void onPause() {
		super.onPause();
		if(camera != null) {
			camera.release();
			camera = null;
		}
	}

}
