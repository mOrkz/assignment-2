package assignment2.cameraapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.HashMap;

/**
 * This activity will display a Snap and add buttons for deleting and sharing
 */
public class ViewImage extends Activity {
    String ID, date, latitude, longitude, country, area, imageFile, description;
    TextView descriptionView;
    ImageView imageView;
    File file;

	/**
	 * Gets a Snap from previous activity, get the data from it, and set it to our view
	 *
	 * @param savedInstanceState The saved instance state
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_image);

        HashMap<String, String> snap = (HashMap<String, String>) getIntent().getSerializableExtra("snap");

        ID = snap.get("ID");
        date = snap.get("date");
        date = date.substring(0, date.length() - 9);
        latitude = snap.get("latitude");
        longitude = snap.get("longitude");
        country = snap.get("country");
        area = snap.get("area");
        imageFile = snap.get("imageFile");

        setViewItems();
	}

	/**
	 * Define view items and set data to them
	 */
    public void setViewItems() {
        descriptionView = (TextView) findViewById(R.id.description);
        description = getString(R.string.share_description_location) + " " + area + ", " + country + " " + getString(R.string.share_description_date) + " " + date;
        descriptionView.setText(description);

        Image image = new Image();
        file = image.getFile(imageFile);
        imageView = (ImageView) findViewById(R.id.image);
        imageView.setImageBitmap(image.setBitmap(file));
    }

    /**
     * Function for sharing the Snap to another application installed on the device
	 * Tutorial followed: http://developer.android.com/training/sharing/send.html
     *
     * @param view The view
     */
    public void share(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        intent.putExtra(Intent.EXTRA_TEXT, description);
        intent.setType("*/*");
        startActivity(Intent.createChooser(intent, getResources().getText(R.string.share_text)));
    }

	/**
	 * Delete a Snap by deleting it from the database and removing the image file
	 *
	 * @param view The view
	 */
    public void delete(View view) {
        DB db = new DB(this);
        db.delete(ID);
        file.delete();
        gotoMain();
    }

    /**
     * Goes back to the main activity
     */
    public void gotoMain() {
        Intent intent = new Intent(getApplication(), Main.class);
        startActivity(intent);
    }

}
