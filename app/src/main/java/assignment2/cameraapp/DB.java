package assignment2.cameraapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Database handler based on SQLite will handle our database transactions
 */
public class DB extends SQLiteOpenHelper {

	/**
	 * Gets the application context and defines our database file
	 *
	 * @param applicationContext
	 */
    public DB(Context applicationContext) {
        super(applicationContext, "snapr.db", null, 1);
    }

	/**
	 * When database is created we'll create a table that will contain information about our snaps
	 *
	 * @param db The database object
	 */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE snaps ( " +
                "ID INTEGER PRIMARY KEY," +
                "date STRING," +
                "latitude DOUBLE," +
                "longitude DOUBLE," +
                "country TEXT," +
                "area TEXT," +
                "imageFile STRING)";
        db.execSQL(query);
    }

	/**
	 * Upgrade the database to make sure it is the latest schema version
	 *
	 * @param db The database object
	 * @param i The old database version
	 * @param i2 The new database version
	 */
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        String query = "DROP TABLE IF EXISTS snaps";
        db.execSQL(query);
        onCreate(db);
        db.close();
    }

	/**
	 * Inserts records about a snap to the database
	 *
	 * @param data Data about a snap
	 */
    public void insert(HashMap<String, String> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("date", data.get("date"));
        values.put("latitude", data.get("latitude"));
        values.put("longitude", data.get("longitude"));
        values.put("country", data.get("country"));
        values.put("area", data.get("area"));
        values.put("imageFile", data.get("imageFile"));

        db.insert("snaps", null, values);
        db.close();
    }

	/**
	 * Delete record from database
	 *
	 * @param ID Id of record to delete
	 */
     public void delete(String ID) {
         SQLiteDatabase db = this.getWritableDatabase();
         db.delete("snaps", "ID = " + ID, null);
         db.close();
     }

	/**
	 * Gets all the records of our stored snaps, loop them out and puts them into an ArrayList
	 *
	 * @return The list of snaps
	 */
    public ArrayList<HashMap<String, String>> getAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM snaps ORDER BY ID DESC";

        ArrayList<HashMap<String, String>> snaps = new ArrayList<HashMap<String, String>>();
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()) {
            do {
                HashMap<String, String> snap = new HashMap<String, String>();
                snap.put("ID", cursor.getString(0));
                snap.put("date", cursor.getString(1));
                snap.put("latitude", cursor.getString(2));
                snap.put("longitude", cursor.getString(3));
                snap.put("country", cursor.getString(4));
                snap.put("area", cursor.getString(5));
                snap.put("imageFile", cursor.getString(6));
                snaps.add(snap);
            } while(cursor.moveToNext());
        }
        db.close();
        return snaps;
    }

}