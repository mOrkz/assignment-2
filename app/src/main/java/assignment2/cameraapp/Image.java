package assignment2.cameraapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.util.Log;

import java.io.File;

/**
 * Image class will handle images so we can call functions from one place
 */
public class Image {

    /**
     * Gets a new file inside the a new folder called Snapr inside of the environments pictures folder
     * If the folder does not exist, create it
     *
     * @param filename The filename
     * @return The file
     */
	public File getFile(String filename) {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File directory = new File(path, "snapr");
        if(!directory.exists()) {
            if(!directory.mkdir()) {
                Log.v("ERROR ", "failed to create directory");
                return null;
            }
        }
        return new File(directory.getPath() + File.separator + filename);
    }

	/**
	 * Set bitmap to a file
	 * Because our images are saved as rotated, rotate them back in our view
	 *
	 * @param file The image file
	 * @return The bitmap of file
	 */
    public Bitmap setBitmap(File file) {
        Matrix matrix = new Matrix();
        matrix.preRotate(90);

        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        return bitmap;
	}

}
