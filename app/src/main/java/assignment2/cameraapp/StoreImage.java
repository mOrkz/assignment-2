package assignment2.cameraapp;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Stores a snap by getting the image and information about current location
 */

public class StoreImage extends Activity{
    private String date, latitude, longitude, country, area, filename;
    LocationManager locationManager;
    public LatLng currentLocation;
    public Double currentLatitude, currentLongitude;

	/**
	 * Gets the image file that was sent from the previous activity, and store it with information about current location
	 * Then go back to main activity
	 *
	 * @param savedInstanceState The saved instance state
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		saveFile(getIntent().getByteArrayExtra("image"));
		setPosition(lastPosition());
		storeInDatabase();

		gotoMain();
	}

	/**
	 * Writes a new image file by setting the filename to current date and time
	 * Because we use the same date variable for our filename, make it filename-friendly by removing the space and replacing it with an underscore
	 *
	 * @param data The file to write and save
	 */
	public void saveFile(byte[] data) {
        date = new SimpleDateFormat("dd.MM.yyyy HH.mm.ss", Locale.getDefault()).format(new Date());
        filename = date.replace(" ", "_") + ".jpg";
        Image image = new Image();
		File file = image.getFile(filename);
		if (file == null){
			Log.v("ERROR ", "on creating media file, check storage permissions");
			return;
		}
		try {
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(data);
			fos.close();
		} catch (FileNotFoundException e) {
			Log.v("ERROR ", "file not found: " + e.getMessage());
		} catch (IOException e) {
			Log.v("ERROR ", "accessing file: " + e.getMessage());
		}
	}

	/**
	 * Sets current location by requesting it from the location manager, by network
	 * If location was unobtainable, show a toast error message to the user
	 *
	 * @param location Current location
	 */
    public void setPosition(Location location) {
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000 * 60, 10, locationListener);

        if(location != null) {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            currentLocation = new LatLng(currentLatitude, currentLongitude);
            getLocationDetails(currentLatitude, currentLongitude);
        } else {
            Toast.makeText(this, "Unable to retrieve location! \n Please check your settings", Toast.LENGTH_LONG).show();
        }
    }

	/**
	 * Gets details about the location by latitude and longitude
	 *
	 * @param lat Latitude
	 * @param lon Longitude
	 */
    public void getLocationDetails(Double lat, Double lon) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if(addresses != null && addresses.size() > 0) {
                Address location = addresses.get(0);
                country = location.getCountryName();
                area = location.getLocality();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(lon);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

	/**
	 * Saves the snap to the database with date, location and image file
	 */
	private void storeInDatabase() {
		DB db = new DB(this);

		HashMap<String, String> snap = new HashMap<String, String>();
        snap.put("date", date);
        snap.put("latitude", latitude);
        snap.put("longitude", longitude);
        snap.put("country", country);
        snap.put("area", area);
        snap.put("imageFile", filename);

		db.insert(snap);
	}

	/**
	 * Attempt to get the last location stored onto the device
	 *
	 * @return The last known location
	 */
	private Location lastPosition() {
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	}

	/**
	 * Start a listener for location so we can update our current location
	 */
	private final LocationListener locationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			setPosition(location);
		}
		public void onStatusChanged(String s, int i, Bundle bundle) {}
		public void onProviderEnabled(String s) {}
		public void onProviderDisabled(String s) {}
	};

	/**
	 * Goes back to the main activity
	 */
	public void gotoMain() {
		Intent intent = new Intent(getApplication(), Main.class);
		startActivity(intent);
	}

}
