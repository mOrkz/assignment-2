package assignment2.cameraapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * This class was written by following the tutorial at: http://developer.android.com/guide/topics/media/camera.html
 * and creates a camera preview for the user
 */
@SuppressLint("all")
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
	private SurfaceHolder holder;
	private Camera camera;

	/**
	 * Get the context and set the camera and add a callback
	 *
	 * @param context Context
	 * @param camera Camera
	 */
	public CameraPreview(Context context, Camera camera) {
		super(context);
		this.camera = camera;

		holder = getHolder();
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        holder.addCallback(this);
	}

	/**
	 * When the preview was drawn we will try to start the preview
	 *
	 * @param surfaceHolder The surface holder
	 */
	@Override
	public void surfaceCreated(SurfaceHolder surfaceHolder) {
		try {
            startPreview();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * When the surface was changed we will try to stop and start the preview again
	 *
	 * @param surfaceHolder The surface holder
	 * @param i PixelFormat of surface
	 * @param i2 Width of surface
	 * @param i3 Height of surface
	 */
	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
		if(holder.getSurface() == null) {
			return;
		}
		try {
			camera.stopPreview();
		} catch(Exception e) {
            e.printStackTrace();
		}

		try {
            startPreview();
		} catch(Exception e) {
            e.printStackTrace();
		}
	}

	/**
	 * Stub
	 *
	 * @param surfaceHolder The surface holder
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder surfaceHolder) {}

	/**
	 * Change screen orientation before attempting to start the camera preview
	 *
	 * @throws Exception
	 */
	public void startPreview() throws Exception {
		camera.setDisplayOrientation(90);
        camera.setPreviewDisplay(holder);
        camera.startPreview();
	}

}
