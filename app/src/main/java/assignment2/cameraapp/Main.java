package assignment2.cameraapp;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Main activity contains a list of all snaps stored on the device
 */
public class Main extends ListActivity {
    ArrayList<HashMap<String, String>> snaps;
	ArrayList<HashMap<String, Object>> snapsList;
	String ID, date, latitude, longitude, country, area, imageFile;
    TextView noSnaps;

	/**
	 * Instantiate database handler and get all snaps stored in the database.
	 * Puts the returned values inside an ArrayList and sets the view adapter
	 *
	 * @param savedInstanceState Saved instance state
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

		DB db = new DB(this);

		snaps = db.getAll();
		snapsList = new ArrayList<HashMap<String, Object>>();

		if(snaps.size() != 0) {
			for(HashMap<String, String> map : snaps) {
                ID = map.get("ID");
				date = map.get("date");
				date = date.substring(0, date.length() - 9);
				latitude = map.get("latitude");
				longitude = map.get("longitude");
				country = map.get("country");
				area = map.get("area");
				imageFile = map.get("imageFile");

				snapsList.add(getViewItems());
			}
		}  else {
            noSnaps = (TextView) findViewById(R.id.noSnaps);
            noSnaps.setVisibility(View.VISIBLE);
        }

		setListAdapter(getListAdapter(snapsList));
	}

	/**
	 * Sets the list adapter so we can populate a listview. We will set a view binder so we can get our images to the view
	 *
	 * @param snapsList Database records of snaps
	 * @return The adapter
	 */
	public ListAdapter getListAdapter(final ArrayList<HashMap<String, Object>> snapsList) {
		SimpleAdapter adapter = new SimpleAdapter(
				Main.this, snapsList, R.layout.list_image,
				new String[] {"image", "snapped"},
				new int[] {R.id.imageView, R.id.imageLabelTextView}
		);

		adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
			@Override
			public boolean setViewValue(View view, Object object, String string) {
				if (view instanceof TextView) {
					((TextView) view).setText(string);
					return true;
				} else if ((view instanceof ImageView) & (object instanceof Bitmap)) {
					((ImageView) view).setImageBitmap((Bitmap) object);
					return true;
				}
				return false;
			}
		});

		return adapter;
	}

	/**
	 * Handle clicks on the list. Get the item in position of arraylist and send the item to a new activity
	 *
	 * @param l The ListView
	 * @param v The view
	 * @param position Position of ArrayList
	 * @param id Row id of item
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

		Intent intent = new Intent(getApplication(), ViewImage.class);
		intent.putExtra("snap", snaps.get(position));
		startActivity(intent);
	}

	/**
	 * Formats our strings nicely and gets the image stored inside the database by getting the file
	 * Puts it all inside a HashMap
	 *
	 * @return The snap
	 */
	public HashMap<String, Object> getViewItems() {
		HashMap<String, Object> item = new HashMap<String, Object>();
        item.put("snapped", getString(R.string.snapped_on) + " " + date + " " + getString(R.string.at) + " " + area + ", " + country);

        Image image = new Image();
        File file = image.getFile(imageFile);
        if(!file.exists()) {
            Log.v("ERROR ", "could not find file");
            return null;
        }

        item.put("image", image.setBitmap(file));

		return item;
	}

	/**
	 * Starts the SnapImage activity
	 *
	 * @param view The view
	 */
    public void StartSnapImage(View view) {
        Intent intent = new Intent(getApplication(), SnapImage.class);
		startActivity(intent);
    }
}
